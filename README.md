## Before start
### Required parameters:
--url or -U: Specify the website URL

**Example** 
```
pytest -U https://www.youtube.com
```
**Alternative**

If you prefer not to use the command-line parameter:
Open the conftest.py file.
Locate the pytest_addoption function.
Find the url option and set the default value to your desired website URL.
Example:

```
def pytest_addoption(parser):
    parser.addoption("--url", "-U", action="store", default="https://www.youtube.com/", help="choose your browser")
```
### Required 
Create a config.ini file with the format data in it:
```commandline
[LoginData]
login=YOUR_EMAIL
password=YOUR_PASSWORD
```