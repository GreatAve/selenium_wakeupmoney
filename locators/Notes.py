from collections import namedtuple
from dataclasses import dataclass


@dataclass
class Notes:
    NotesLocators = namedtuple("NotesLocators", ("locator_type", "locator"))
    # Buttons
    add_note_button = NotesLocators("css", ".notes-container .add-note")
    add_note_big_button = NotesLocators("css", ".notes-button")
    delete_button = NotesLocators("css", ".note-title-delete")

    # Fields
    header_input = NotesLocators("css", ".ce-header")
    body_input = NotesLocators("css", ".ce-paragraph")
    exist_header_input = NotesLocators("css", ".note-title")



