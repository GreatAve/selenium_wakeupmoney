from collections import namedtuple
from dataclasses import dataclass


@dataclass
class CommonHeader:
    HeaderLocator = namedtuple("HeaderLocator", ("locator_type", "locator"))
# In this context, treat a link as a button
    invest_link = HeaderLocator("css", "[href='/invest/']")
    projects_link = HeaderLocator("css", "[href='/projects/']")
    notes_link = HeaderLocator("css", "[href = '/notes/']")
    goals_link = HeaderLocator("css", "[href='/goals/']")
