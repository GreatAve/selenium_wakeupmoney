import dataclasses
from collections import namedtuple
from dataclasses import dataclass


@dataclass
class Login:
    LoginLocators = namedtuple("LoginLocators", ("locator_type", "locator"))
    email_input = LoginLocators("css", "input[type=email]")
    password_input = LoginLocators("css", "#password-input")
    login_button = LoginLocators("css", "#login_submit")
