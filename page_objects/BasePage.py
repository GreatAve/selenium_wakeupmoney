from collections import namedtuple
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def __element(
        self, selector: namedtuple, index: int, link_text: str = None
    ) -> str | list:
        """
        :param selector: Named tuple whose first value is the locator_type. The second value is the locator itself.
        :param index: Order number of element from the list of found elements
        :param link_text: Text that will expect visibility
        :return: Element from the list of found elements, whose index is the parameter of this function.
                 If index is None returns an elements list
        """
        by = None
        if link_text:
            by = By.LINK_TEXT
            selector = selector.locator
        elif "css" in selector:
            by = By.CSS_SELECTOR
            selector = selector.locator
        elif "class" in selector:
            by = By.CLASS_NAME
            selector = selector.locator
        if index is None:
            return self.driver.find_elements(by, selector)
        else:
            return self.driver.find_elements(by, selector)[index]

    def _click(self, selector, index=0):
        """
        Move mouse to the item and click (I don't know why, but let it be)
        """
        ActionChains(self.driver).move_to_element(
            self.__element(selector, index)
        ).click().perform()

    def _input(self, selector: namedtuple, value: str, index: int = 0):
        element = self.__element(selector, index)
        element.clear()
        element.send_keys(value)

    def _wait_for_visible(
        self, selector: namedtuple, link_text: str = None, index: int = 0, wait: int = 3
    ):
        return WebDriverWait(self.driver, wait).until(
            ec.visibility_of(self.__element(selector, index, link_text))
        )

    def _get_element_text(self, selector: namedtuple, index: int):
        return self.__element(selector, index).text

    def _fill_broken_fields(self, selector: namedtuple, value: str, index=0):
        """
        For fields that are not activated by WebDriver methods only. (require a mouse click)
        """
        element = self.__element(selector, index)
        element.click()
        element.send_keys(value)

    def _get_elements(self, selector: namedtuple, index: int = None) -> list:
        return self.__element(selector, index)

    def _simple_click(self, selector: namedtuple):
        self.selector = selector
        self.selector.click()
