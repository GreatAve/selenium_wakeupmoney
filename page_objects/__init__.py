from .BasePage import BasePage
from .LoginPage import LoginPage
from .NotesPage import NotesPage
from .common import TopMenu
