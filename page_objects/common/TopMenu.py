from page_objects import BasePage
from locators import CommonHeader


class TopMenu(BasePage):
    def click_invest(self):
        self._wait_for_visible(CommonHeader.invest_link, index=1)
        self._click(CommonHeader.invest_link, index=1)
        return self

    def click_projects(self):
        self._wait_for_visible(CommonHeader.projects_link, index=1)
        self._click(CommonHeader.projects_link, index=1)
        return self

    def click_notes(self):
        self._wait_for_visible(CommonHeader.notes_link, index=1)
        self._click(CommonHeader.notes_link, index=1)
        return self

    def click_goals(self):
        self._wait_for_visible(CommonHeader.goals_link, index=1)
        self._click(CommonHeader.goals_link, index=1)
        return self

