from collections import namedtuple
from page_objects import BasePage
from locators import Notes
from faker import Faker


faker = Faker(["ru_RU"])


class NotesPage(BasePage):
    @staticmethod
    def _fake_data():
        FakeData = namedtuple("FakeData", ("fake_header", "fake_text"))
        return FakeData(faker.company(), faker.text())

    def create_new_note(self):
        self._wait_for_visible(Notes.add_note_big_button, wait=1)
        self._click(Notes.add_note_big_button)
        return self

    def create_another_note(self):
        self._wait_for_visible(Notes.add_note_button, wait=1)
        print()
        self._click(Notes.add_note_button)
        return self

    def create_note(self):
        try:
            self.create_new_note()
            self.fill_note()
        except IndexError:
            self.create_another_note()
            self.fill_note()

    def fill_note(self, data: namedtuple):
        self._fill_broken_fields(Notes.header_input, data.fake_header)
        self._fill_broken_fields(Notes.body_input, data.fake_text)
        return self

    def list_of_notest(self):
        self._wait_for_visible(Notes.delete_button, wait=1)
        return self._get_elements(Notes.exist_header_input)

    def delete_note(self):
        self._click(Notes.delete_button)
        return self

    def get_elements_headers(self) -> list[str]:
        return [element.text for element in self.list_of_notest()]

