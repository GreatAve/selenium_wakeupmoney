from page_objects import BasePage
from locators import Login


class LoginPage(BasePage):
    def login_user(self, email, password):
        self._input(Login.email_input, email)
        self._input(Login.password_input, password)
        self._click(Login.login_button)
        return self
