import time
from page_objects import LoginPage, TopMenu, NotesPage


def test_login(driver, config, url):
    driver.get(url)
    LoginPage(driver).login_user(email=config.login, password=config.password)
    time.sleep(3)


def test_create_two_notes(browser, config):
    TopMenu(browser).click_notes()

    fake_data_first = NotesPage._fake_data()
    fake_data_second = NotesPage._fake_data()
    while fake_data_first.fake_header == fake_data_second.fake_header:
        fake_data_second = NotesPage._fake_data()

    try:
        NotesPage(browser).create_new_note()
    except IndexError:
        NotesPage(browser).create_another_note()
    finally:
        NotesPage(browser).fill_note(fake_data_first)
    # Need a delay, as there is a BUG: created note replaces the previous one.
    time.sleep(1)
    NotesPage(browser) \
        .create_another_note().fill_note(fake_data_second)
    # Need a delay. It does not have time to process the creation of an element, it is not included in the list
    time.sleep(1)
    elements = NotesPage(browser).get_elements_headers()

    assert fake_data_first.fake_header in elements, \
        f"[ERROR]: Created element '{fake_data_first.fake_header}' not in Notes list"
    assert fake_data_second.fake_header in elements, \
        f"[ERROR]: Created element '{fake_data_second.fake_header}' not in Notes list"


def test_del_from_fixture(browser, config, delete_notes):
    try:
        list_of_notes = NotesPage(browser).list_of_notest()
    except IndexError:
        list_of_notes = []
    assert not list_of_notes, \
        f"[ERROR] Not all notes are removed from the list. Exist:\n{list_of_notes}"
