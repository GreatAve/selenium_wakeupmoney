import pytest
import pickle
import time
import os
from selenium import webdriver
from configparser import ConfigParser
from collections import namedtuple
from page_objects import LoginPage, TopMenu, NotesPage


@pytest.fixture
def config() -> namedtuple:
    config = ConfigParser()
    config.read("config.ini")
    LogPass = namedtuple("LogPass", ("login", "password", "url"))
    return LogPass(
        config.get("LoginData", "login"),
        config.get("LoginData", "password"),
        config.get("Url", "url")
    )


# Additional parameters at start
def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        "-B",
        action="store",
        default="chrome",
        help="Choose your browser type",
    )
    parser.addoption(
        "--url",
        "-U",
        action="store",
        default="YOUR URL HERE",
        help="Insert the URL",
    )
    parser.addoption(
        "--maximize",
        "-M",
        action="store",
        default=True,
        help="Make the browser window full screen?",
    )


@pytest.fixture
def url(request) -> str:
    return request.config.getoption("--url")


@pytest.fixture
def config() -> namedtuple:
    config = ConfigParser()
    config.read("config.ini")
    LogPass = namedtuple("LogPass", ("login", "password"))
    return LogPass(
        config.get("LoginData", "login"), config.get("LoginData", "password")
    )


@pytest.fixture
def driver(request, url: str) -> webdriver:
    """Browser init"""
    browser = request.config.getoption("--browser")
    if browser.lower() == "chrome":
        driver = webdriver.Chrome()
    elif browser.lower() == "firefox":
        driver = webdriver.Firefox()
    elif browser.lower() == "safari":
        driver = webdriver.Safari()
    else:
        raise Exception(f"{request.param} is not supported!")
    return driver


@pytest.fixture
def cookies(request, driver: webdriver, config: namedtuple, url: str) -> list:
    if os.path.exists("cookies.pickle"):
        with open("cookies.pickle", "rb") as cookies_byte:
            return pickle.load(cookies_byte)
    else:
        driver.get(url)
        LoginPage(driver).login_user(email=config.login, password=config.password)
        cookies = driver.get_cookies()
        pickle.dump(cookies, open("cookies.pickle", "wb"))
        request.addfinalizer(driver.close)
        return cookies


@pytest.fixture
def browser(request, url: str, driver: webdriver, cookies: list) -> webdriver:
    browser_maximize = request.config.getoption("--maximize")
    driver.implicitly_wait(3)

    def _open(path=""):
        return driver.get(url + path)

    if browser_maximize:
        driver.maximize_window()

    driver.open = _open
    driver.open()
    for cookie in cookies:
        driver.add_cookie(cookie)

    driver.refresh()
    request.addfinalizer(driver.close)
    return driver


@pytest.fixture
def delete_notes(browser: webdriver, config: namedtuple):
    TopMenu(browser).click_notes()

    def _del_note(elements: list):
        """
        Terrible fixture to get around the low-priority deletion bug
        """
        for _ in elements:
            NotesPage(browser).delete_note()
            time.sleep(0.5)

    def trying_to_find_elements():
        try:
            elements = NotesPage(browser).list_of_notest()
            if elements:
                _del_note(elements)
        except IndexError:
            pass

    trying_to_find_elements()
    # Temporary solution while note deletion is broken
    time.sleep(2)
    trying_to_find_elements()
